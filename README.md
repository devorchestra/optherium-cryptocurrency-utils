# Project description

**optherium-cryptocurrency-utils** library is designed as an auxiliary entity for dealing with a limited list of cryptocurrencies. It contains all necessary information about coins, tokens and networks, supported by Optherium team.  

# Installation

1. Add the following to `package.json` into `dependencies` section:
    ```
    "optherium-cryptocurrency-utils": "bitbucket:devorchestra/optherium-cryptocurrency-utils#VERSION"
    ```
2. Replace `VERSION` with version number, for example `1.0.5`.
    
3. Run `npm install`.

# Component description

### info.ts

Provides necessary info about cryptocurrencies (coins and tokens), which are currently supported by Optherium organisation.

### networks.ts

Contains information about blockchain networks.

### WalletFactory.ts

The factory is developed for wallet generating and transaction signing.