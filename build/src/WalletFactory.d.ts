import { ITxMessage, IWallet } from "./interfaces";
declare class WalletFactory {
    static convertLtc(oldAddress: any): string;
    static generateWallet(currency: string, environment: string): IWallet;
    static signMultiSigTx(wallet: IWallet, tx: ITxMessage, environment: string): any[] | {
        sigV: any;
        sigR: string;
        sigS: string;
    };
}
export default WalletFactory;
