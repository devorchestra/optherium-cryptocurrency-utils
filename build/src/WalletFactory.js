"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cryptocurrency = require("bitcoinjs-lib");
const networks_1 = require("./networks");
let Wallet = require('ethereumjs-wallet');
let ethUtil = require("ethereumjs-util");
class WalletFactory {
    static convertLtc(oldAddress) {
        let decoded = cryptocurrency.address.fromBase58Check(oldAddress);
        let version = decoded['version'];
        switch (version) {
            case 5:
                version = 50;
                break;
            case 50:
                version = 5;
                break;
            case 196:
                version = 58;
                break;
            case 58:
                version = 196;
                break;
            default:
                throw "unknown";
        }
        return cryptocurrency.address.toBase58Check(decoded['hash'], version);
    }
    static generateWallet(currency, environment) {
        let key;
        let wallet;
        switch (currency.toUpperCase()) {
            case "ETH":
                currency = "ethereum";
                break;
            case "BTC":
                currency = "bitcoin";
                break;
            case "LTC":
                currency = "litecoin";
                break;
            case "DASH":
                currency = "dash";
                break;
        }
        if (currency == 'ethereum') {
            key = Wallet.generate();
            wallet = {
                currency: currency,
                address: key.getAddress().toString('hex'),
                publicKey: key.getPublicKey().toString('hex'),
                privateKey: key.getPrivateKey().toString('hex')
            };
        }
        else {
            key = cryptocurrency.ECPair.makeRandom({ network: networks_1.default[environment][currency.toLowerCase()] });
            wallet = {
                currency: currency,
                address: key.getAddress(),
                publicKey: key.getPublicKeyBuffer().toString('hex'),
                privateKey: key.toWIF()
            };
        }
        return wallet;
    }
    static signMultiSigTx(wallet, tx, environment) {
        if (wallet.currency.toLowerCase() === 'ethereum' || wallet.currency.toLowerCase() === 'eth') {
            let sig = ethUtil.ecsign(new Buffer(ethUtil.stripHexPrefix(tx.msgHash), 'hex'), new Buffer(wallet.privateKey, 'hex'));
            return {
                sigV: sig.v,
                sigR: '0x' + sig.r.toString('hex'),
                sigS: '0x' + sig.s.toString('hex')
            };
        }
        else {
            let networkCurrency = wallet.currency.toLowerCase();
            let txb = cryptocurrency.TransactionBuilder.fromTransaction(cryptocurrency.Transaction.fromHex(tx.msgHash), networks_1.default[environment][networkCurrency]);
            let pubKeys = [Buffer.from(tx.servicePublic, 'hex'), Buffer.from(tx.ownerPublic, 'hex'), Buffer.from(tx.krsPublic, 'hex')];
            let redeemScript = cryptocurrency.script.multisig.output.encode(2, pubKeys);
            let secretKey = cryptocurrency.ECPair.fromWIF(wallet.privateKey, networks_1.default[environment][networkCurrency]);
            let signatures = [];
            txb.tx.ins.forEach((ins, i) => {
                txb.sign(i, secretKey, redeemScript);
                signatures.push(txb.inputs[i].signatures[1].toString('hex'));
            });
            return signatures;
        }
    }
}
exports.default = WalletFactory;
