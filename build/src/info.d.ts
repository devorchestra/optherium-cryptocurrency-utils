declare const info: {
    development: {
        coins: any;
        tokens: any;
        fiat: any;
    };
    loadtest: {
        coins: any;
        tokens: any;
        fiat: any;
    };
    stage: {
        coins: any;
        tokens: any;
        fiat: any;
    };
    production: {
        coins: any;
        tokens: any;
        fiat: any;
    };
};
export default info;
