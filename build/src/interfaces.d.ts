export interface IWallet {
    currency: string;
    address: string;
    publicKey: string;
    privateKey: string;
}
export interface ITxMessage {
    msgHash: string;
    ownerPublic: string;
    servicePublic: string;
    krsPublic: string;
    ownerSignatures?: string[];
}
