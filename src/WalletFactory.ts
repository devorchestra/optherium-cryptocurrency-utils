import cryptocurrency = require('bitcoinjs-lib');

import {ITxMessage, IWallet} from "./interfaces";
import networks from "./networks";

let Wallet = require('ethereumjs-wallet');
let ethUtil = require("ethereumjs-util");

class WalletFactory {

    public static convertLtc(oldAddress) {
        let decoded = cryptocurrency.address.fromBase58Check(oldAddress);
        let version = decoded['version'];
        switch (version) {
            case 5:
                version = 50;
                break;
            case 50:
                version = 5;
                break;
            case 196:
                version = 58;
                break;
            case 58:
                version = 196;
                break;
            default:
                throw "unknown";
        }
        return cryptocurrency.address.toBase58Check(decoded['hash'], version);
    }

    public static generateWallet(currency: string, environment: string) {
        let key;
        let wallet: IWallet;
        switch (currency.toUpperCase()) {
            case "ETH":
                currency = "ethereum";
                break;
            case "BTC":
                currency = "bitcoin";
                break;
            case "LTC":
                currency = "litecoin";
                break;
            case "DASH":
                currency = "dash";
                break;
        }

        if (currency == 'ethereum') {
            key = Wallet.generate();
            wallet = {
                currency: currency,
                address: key.getAddress().toString('hex'),
                publicKey: key.getPublicKey().toString('hex'),
                privateKey: key.getPrivateKey().toString('hex')
            };
        } else {
            key = cryptocurrency.ECPair.makeRandom({network: networks[environment][currency.toLowerCase()]});
            wallet = {
                currency: currency,
                address: key.getAddress(),
                publicKey: key.getPublicKeyBuffer().toString('hex'),
                privateKey: key.toWIF()
            };
        }
        return wallet
    }

    public static signMultiSigTx(wallet: IWallet, tx: ITxMessage, environment: string) {
        if (wallet.currency.toLowerCase() === 'ethereum' || wallet.currency.toLowerCase() === 'eth') {
            let sig = ethUtil.ecsign(new Buffer(ethUtil.stripHexPrefix(tx.msgHash), 'hex'), new Buffer(wallet.privateKey, 'hex'));
            return {
                sigV: sig.v,
                sigR: '0x' + sig.r.toString('hex'),
                sigS: '0x' + sig.s.toString('hex')
            };
        } else {
            let networkCurrency = wallet.currency.toLowerCase();
            let txb = cryptocurrency.TransactionBuilder.fromTransaction(cryptocurrency.Transaction.fromHex(tx.msgHash), networks[environment][networkCurrency]);
            let pubKeys = [Buffer.from(tx.servicePublic, 'hex'), Buffer.from(tx.ownerPublic, 'hex'), Buffer.from(tx.krsPublic, 'hex')];
            let redeemScript = cryptocurrency.script.multisig.output.encode(2, pubKeys);
            let secretKey = cryptocurrency.ECPair.fromWIF(wallet.privateKey, networks[environment][networkCurrency]);
            let signatures = [];
            txb.tx.ins.forEach((ins, i) => {
                txb.sign(i, secretKey, redeemScript);
                signatures.push(txb.inputs[i].signatures[1].toString('hex'));
            });
            return signatures;
        }

    }
}

export default WalletFactory