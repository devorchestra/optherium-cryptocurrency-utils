let realInfo = {
    coins: {
        btc: {
            multiplier: 100000000,
            decimal: 8,
            fullName: "Bitcoin",
            firstColor: '#ffc555',
            secondColor: '#ffb11a',
            totalSupply: 17062175
        },
        // bch: {
        //     multiplier: 100000000,
        //     decimal: 8,
        //     fullName: "Bitcoin Cash",
        //     firstColor: '#11c49c',
        //     secondColor: '#03a782',
        //     totalSupply: 17154088
        // },
        ltc: {
            multiplier: 100000000,
            decimal: 8,
            fullName: "Litecoin",
            firstColor: '#919191',
            secondColor: '#4e4e4e',
            totalSupply: 56746648
        },
        dash: {
            multiplier: 100000000,
            decimal: 8,
            fullName: "Dash",
            firstColor: '#459ee4',
            secondColor: '#1d75bb',
            totalSupply: 8097725
        },
        eth: {
            multiplier: 1000000000000000000,
            decimal: 18,
            fullName: "Ethereum",
            firstColor: '#4f79b2',
            secondColor: '#375884',
            totalSupply: 99740116
        }
    },
    tokens: {
        opex: {
            multiplier: 1000000000000000000,
            fullName: "Optherium",
            address: "0x0b34a04b77aa9bd2c07ef365c05f7d0234c95630",
            symbol: "OPEX",
            decimal: 18,
            type: "default",
            firstColor: '#301992',
            secondColor: '#3274bd',
            totalSupply: 3200000000
        },
        omg: {
            multiplier: 1000000000000000000,
            fullName: "OmiseGO",
            address: "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07",
            symbol: "OMG",
            decimal: 18,
            type: "default",
            firstColor: '#5380ff',
            secondColor: '#1a52ef',
            totalSupply: 102042552
        },
        snt: {
            multiplier: 1000000000000000000,
            fullName: "Status",
            address: "0x744d70FDBE2Ba4CF95131626614a1763DF805B9E",
            symbol: "SNT",
            decimal: 18,
            type: "default",
            firstColor: '#6474d1',
            secondColor: '#4a5ab5',
            totalSupply: 3470483788
        }
    },
    fiat: {
        usd: {
            fullName: "United States Dollar"
        }
    }
};

let fakeInfo = {
    coins: {
        btc: {
            multiplier: 100000000,
            decimal: 8,
            fullName: "Bitcoin",
            firstColor: '#ffc555',
            secondColor: '#ffb11a',
            totalSupply: 17062175
        },
        ltc: {
            multiplier: 100000000,
            decimal: 8,
            fullName: "Litecoin",
            firstColor: '#919191',
            secondColor: '#4e4e4e',
            totalSupply: 56746648
        },
        eth: {
            multiplier: 1000000000000000000,
            decimal: 18,
            fullName: "Ethereum",
            firstColor: '#4f79b2',
            secondColor: '#375884',
            totalSupply: 99740116
        }
    },
    tokens: {
        opex: {
            multiplier: 1000000000000000000,
            fullName: "Optherium",
            address: "0x83b9956ff86e857a9f597f80435c552a87eb164c",
            symbol: "OPEX",
            decimal: 18,
            type: "default",
            firstColor: '#301992',
            secondColor: '#3274bd',
            totalSupply: 900000000
        },
        eos: {
            multiplier: 1000000000000000000,
            fullName: "EOSIO",
            address: "0xca275b66df285b8f9e391db9a79fe750b585aaee",
            symbol: "EOS",
            decimal: 18,
            type: "default",
            firstColor: '#4c4849',
            secondColor: '#231f20',
            totalSupply: 900000000
        },
        trx: {
            multiplier: 1000000000000000000,
            fullName: "TRON",
            address: "0x1177d62bf93766998b96bace1684e5299abef469",
            symbol: "TRX",
            decimal: 18,
            type: "default",
            firstColor: '#da6058',
            secondColor: '#bc362c',
            totalSupply: 65748111645
        },
        omg: {
            multiplier: 1000000000000000000,
            fullName: "OmiseGO",
            address: "0x232bb0cf3fd241116186684350c526b7e48f7810",
            symbol: "OMG",
            decimal: 18,
            type: "default",
            firstColor: '#5380ff',
            secondColor: '#1a52ef',
            totalSupply: 102042552
        },
        snt: {
            multiplier: 1000000000000000000,
            fullName: "Status",
            address: "0x713c97fc989b2f5c48fd93084df10301fbd99d7c",
            symbol: "SNT",
            decimal: 18,
            type: "default",
            firstColor: '#6474d1',
            secondColor: '#4a5ab5',
            totalSupply: 3470483788
        }
    },
    fiat: {
        usd: {
            fullName: "United States Dollar"
        }
    }
};

const info: {
    development: { coins: any, tokens: any, fiat: any },
    loadtest: { coins: any, tokens: any, fiat: any },
    stage: { coins: any, tokens: any, fiat: any },
    production: { coins: any, tokens: any, fiat: any }
} = {
    development: fakeInfo,
    loadtest: fakeInfo,
    stage: realInfo,
    production: realInfo
};

export default info

