let realNetworks = {
    bitcoin: {
        messagePrefix: '\x18Bitcoin Signed Message:\n',
        bech32: 'bc',
        bip32: {
            public: 0x0488b21e,
            private: 0x0488ade4
        },
        pubKeyHash: 0x00,
        scriptHash: 0x05,
        wif: 0x80
    },
    litecoin: {
        messagePrefix: '\x19Litecoin Signed Message:\n',
        bip32: {
            public: 0x019da462,
            private: 0x019d9cfe
        },
        pubKeyHash: 0x30,
        scriptHash: 0x32,
        wif: 0xb0
    },
    dash: {
        messagePrefix: '\x19Dash Signed Message:\n',
        bip32: {
            public: 0x488b21e,
            private: 0x488ade4
        },
        pubKeyHash: 0x4c,
        scriptHash: 0x10,
        wif: 0xcc
    }
};

let fakeNetworks = {
    bitcoin: {
        messagePrefix: '\x18Bitcoin Signed Message:\n',
        bech32: 'tb',
        bip32: {
            public: 0x043587cf,
            private: 0x04358394
        },
        pubKeyHash: 0x6f,
        scriptHash: 0xc4,
        wif: 0xef
    },
    litecoin: {
        messagePrefix: '\x19Litecoin Signed Message:\n',
        bip32: {
            public: 0x0436f6e1,
            private: 0x0436ef7d
        },
        pubKeyHash: 0x6f,
        scriptHash: 0x3a,
        wif: 0xef
    },
    dash: {
        messagePrefix: '\x19Dash Signed Message:\n',
        bip32: {
            public: 0x488b21e,
            private: 0x488ade4
        },
        pubKeyHash: 0x4c,
        scriptHash: 0x10,
        wif: 0xcc
    }
};

let networks = {
    development: fakeNetworks,
    loadtest: fakeNetworks,
    stage: realNetworks,
    production: realNetworks
};
export default networks